import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)

companies = c.model('res.partner').browse(['|', ('representative_dni', '!=', False), ('representative_name', '!=', False)])
for company in companies:
  try:
    if company.representative_dni:
      representatives = c.model('res.partner').browse([('vat', '=', company.representative_dni)])
      if representatives:
        for rep in representatives:
          if rep.id != company.id:
            rep.representative = True
            if not rep.parent_id:
              rep.parent_id = company.id
        continue

    representatives = c.model('res.partner').browse([('name', '=', company.representative_name)])
    if representatives:
      for rep in representatives:
        if rep.id != company.id:
          rep.representative = True
          if not rep.parent_id:
            rep.parent_id = company.id
  except Exception as err:
    print("ERROR: exception recursive parent for COMPANY: " + str(company.id) + " (can be ignored)")
    continue
