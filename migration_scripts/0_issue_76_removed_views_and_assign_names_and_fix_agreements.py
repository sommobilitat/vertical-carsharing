import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)
views = c.model('ir.ui.view').browse(['|', ('name', 'ilike', 'cs_usage_res_config_settings_view_form'), ('name', 'ilike', 'vertical_cs_res_config_settings_view_form')])
for view in views:
  view.unlink()

partners = c.model('res.partner').browse([('name', '=', '')])

for partner in partners:
  if partner.firstname:
    partner.name = partner.firstname
  elif partner.email:
    partner.name = partner.email
  elif partner.vat:
    partner.name = partner.vat
  else:
    partner.name = 'No name found'

agreements = c.model('agreement').browse([('active', '=', True)])
i = 0
for agreement in agreements:
  agreement.code = i
  i = i + 1