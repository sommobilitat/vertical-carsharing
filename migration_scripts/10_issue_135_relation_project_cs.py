import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)


### FLOTA ###
task_type_flota = c.model('project.type').browse([('name', '=', 'Flota')])

if not task_type_flota:
  raise RuntimeError('Flota task type not found!')

project_gestio_flota = c.model('project.project').browse([('name', '=', 'Gestió Flota')])

if not project_gestio_flota:
  raise RuntimeError('Project Gestió Flota not found!')

project_cars = c.model('project.project').browse([('type_id', '=', task_type_flota[0].id)])

for project in project_cars:
  analytic_account = c.model('account.analytic.account').browse([('name', 'ilike', project.name)])
  car = c.model('fleet.vehicle').browse([('analytic_account_id', '=', analytic_account.id)])
  if car:
    for task in project.task_ids:
      task.project_id = project_gestio_flota[0].id
      task.cs_task_type = 'car'
      task.cs_task_car_id = car[0].id

### COMUNITATS ###
task_type_comunitat = c.model('project.type').browse([('name', '=', 'Comunitat (Aparcament)')])

if not task_type_comunitat:
  raise RuntimeError('Comunitat task type not found!')

project_gestio_comunitats = c.model('project.project').browse([('name', '=', 'Gestió Comunitats')])

if not project_gestio_comunitats:
  raise RuntimeError('Project Gestió Comunitats not found!')

project_comunitats = c.model('project.project').browse([('type_id', '=', task_type_comunitat[0].id)])

for project in project_comunitats:
  analytic_account = c.model('account.analytic.account').browse([('name', 'ilike', project.name)])
  comunitat = c.model('sm_carsharing_structure.cs_community').browse([('analytic_account_id', '=', analytic_account.id)])
  if comunitat:
    for task in project.task_ids:
      task.project_id = project_gestio_comunitats[0].id
      task.cs_task_type = 'community'
      task.cs_task_community_id = comunitat[0].id
