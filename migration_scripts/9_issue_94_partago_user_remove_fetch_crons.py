import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)

crons = c.model('ir.cron').browse([('name', '=', 'Fetch and complete cs update data')])
for cron in crons:
  cron.unlink()