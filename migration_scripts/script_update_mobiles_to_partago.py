import erppeek
import credentials as cred
import requests
import json

c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)

api_url = cred.api_url
headers = {'Content-type': 'application/json', 'apiKey': cred.apikey}

def get_person_api(cs_person_index):
  url = api_url + 'persons/' + cs_person_index
  response = requests.get(url, headers=headers)
  return response

def patch_person_api(cs_person_index, person):
  url = api_url + 'persons/' + cs_person_index
  response = requests.patch(url, headers=headers, data=person)
  return response

partners = c.model('res.partner').browse([('cs_person_index', '!=', False)])

for partner in partners:
  patch_person = True
  if not partner.mobile:
    patch_person = False
    print("WARNING: No mobile for Partner ID: " + str(partner.id))
    continue
  response = get_person_api(partner.cs_person_index)
  if response.status_code != 200:
    patch_person = False
    print("ERROR: GET person API response: " + response.text + " For Partner ID: " + str(partner.id))
    continue
  person = response.json()
  if person.get('phones'):
    person['phones']['main'] = partner.mobile
  else:
    person['phones'] = {'main': partner.mobile}
    #print("WARNING: No phones in carsharing for partner ID: " + str(partner.id))
  person.pop('id')
  if patch_person:
    response = patch_person_api(partner.cs_person_index, json.dumps(person))
    if response.status_code != 200:
      print("ERROR: PATCH person API response: " + response.text + " For Partner ID: " + str(partner.id))
    # print(response.json())
