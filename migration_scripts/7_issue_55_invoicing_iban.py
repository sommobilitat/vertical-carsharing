import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)

def create_iban_account(iban_number, partner_id):
  account = {
    "acc_number": iban_number,
    "acc_type": 'iban',
    "partner_id": partner_id,
  }
  return c.model('res.partner.bank').create(account)

def get_sanitized_iban(partner):
  if partner.invoicing_iban:
    return partner.invoicing_iban
  elif partner.iban_1:
    return partner.iban_1 + partner.iban_2 + partner.iban_3 + partner.iban_4 + partner.iban_5 + partner.iban_6
  elif partner.bank_account_nr_1 and partner.bank_account_type == "account_nr":
    return ('CC - ' + str(partner.bank_account_nr_1) + str(partner.bank_account_nr_2) + str(partner.bank_account_nr_3) + str(partner.bank_account_nr_4) +'')

def process_partners(partners):
  for partner in partners:
    if partner.parent_id: continue
    bank = c.model('res.partner.bank').browse([('partner_id', '=', partner.id)])
    if not bank:
      iban = get_sanitized_iban(partner)
      created = create_iban_account(iban, partner.id)
      if not created:
        print("ERROR: Could not create bank account for partner: " + str(partner.id) + " and IBAN: " + iban)
    else:
        bank.force_onchange_acc_number_base_bank_from_iban()

partners = c.model('res.partner').browse(['|', ('invoicing_iban', '!=', False), ('invoicing_iban', '!=', '')])

process_partners(partners)

partners = c.model('res.partner').browse(['&',
  '|', ('invoicing_iban', '=', False), ('invoicing_iban', '=', ''),
  '&', ('iban_1', '!=', False), ('iban_1', '!=', ''),
  '&', ('iban_2', '!=', False), ('iban_2', '!=', ''),
  '&', ('iban_3', '!=', False), ('iban_3', '!=', ''),
  '&', ('iban_4', '!=', False), ('iban_4', '!=', ''),
  '&', ('iban_5', '!=', False), ('iban_5', '!=', ''),
  '&', ('iban_6', '!=', False), ('iban_6', '!=', '')
  ])

process_partners(partners)

partners = c.model('res.partner').browse(['&',
  '|', ('invoicing_iban', '=', False), ('invoicing_iban', '=', ''),
  '&', ('bank_account_nr_1', '!=', False), ('bank_account_nr_1', '!=', ''),
  '&', ('bank_account_nr_2', '!=', False), ('bank_account_nr_2', '!=', ''),
  '&', ('bank_account_nr_3', '!=', False), ('bank_account_nr_3', '!=', ''),
  '&', ('bank_account_nr_4', '!=', False), ('bank_account_nr_4', '!=', '')
  ])

process_partners(partners)

banks = c.model('res.partner.bank').browse([('bank_id', '=', False)])
for bank in banks:
  if '<p>' in bank.acc_number:
    try:
      bank.acc_number = bank.acc_number.replace('<p>', '').replace('</p>', '')
    except Exception as err:
      print("ERROR: There's another bank with the same account number: " + str(bank.acc_number) + "HAS PARTNER: " + str(bank.partner_id.name))
  if bank.bank_id == False:
    bank.force_onchange_acc_number_base_bank_from_iban()


# TODO: Iterate trough all partner and foreach res.partner.bank execute:
# related_bank._onchange_acc_number_base_bank_from_iban()
