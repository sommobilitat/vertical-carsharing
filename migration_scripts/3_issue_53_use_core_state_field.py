import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)
partners = c.model('res.partner').browse(['|', ('state', '!=', False), ('computed_state', '!=', False)])

# Partners without state or state = catalonia. Setting Barcelona by default
specific_partners = [1866, 1668, 671]

for partner in partners:

  if partner.computed_state:
    states = c.model('res.country.state').browse([('name', 'ilike', partner.computed_state)])
  
  if partner.state and not states:
    states = c.model('res.country.state').browse([('name', 'ilike', partner.state)])

  if states:
    if len(states) == 1:
      partner.state_id = states[0].id
    elif partner.id not in (5651, 3947):
      print("ERROR: Multiple possible states found for partner: " + str(partner.id) + "\nStates: " + str(states) )
  
  # Specific cases
  elif partner.id in specific_partners:
    partner.state_id = 443 # Barcelona
  elif partner.id == 5651:
    partner.state_id = 433 # Córdoba
  elif partner.id == 743:
    partner.state_id = 417 # Araba/Álava
  elif partner.id == 1697:
    partner.state_id = 424 # Balears
  elif partner.id == 671:
    partner.state_id = 435 # Girona
  elif partner.id == 3947:
    partner.state_id = 442 # La Rioja
  
  # Error logging
  else:
    if partner.state:
      print("ERROR: No state found for partner: " + str(partner.id) + "\nState: " + partner.state)
    if partner.computed_state:
      print("ERROR: No state found for partner: " + str(partner.id) + "\nState: " + partner.computed_state)

partners = c.model('res.partner').browse([('state_id', '!=', False)])

for partner in partners:
  if partner.state_id:
    partner.country_id = partner.state_id.country_id.id
