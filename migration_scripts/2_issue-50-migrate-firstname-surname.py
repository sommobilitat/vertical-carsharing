import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)
# CHANGE FIRSTNAME
partners = c.model('res.partner').browse(['|', ('firstname', '=', False), ('firstname', '=', '')])
for partner in partners: 
  partner.firstname = partner.name
  if partner.lastname:
    # Fix for companies having lastname which they shouldn't
    partner.lastname = None
# CHANGE LASTNAME WITH SURNAME IF PARTNER HAS SURNAME
partners = c.model('res.partner').browse(['|', ('surname', '!=', False), ('surname', '!=', '')])
for partner in partners: partner.lastname = partner.surname
# CHANGE LASTNAME IF PARTNER HAS NO SURNAME BUT HAS FIRST+SECOND
partners = c.model('res.partner').browse(['&', 
  '|', ('surname', '=', False), ('surname', '=', ''), 
  '|', ('first_surname', '!=', False), ('first_surname', '!=', ''),
  '|', ('second_surname', '!=', False), ('second_surname', '!=', '')])
for partner in partners: partner.lastname = partner.first_surname + " " + partner.second_surname
# CHANGE LASTNAME IF PARTNER HAS NO SURNAME AND ONLY FIRST SURNAME
partners = c.model('res.partner').browse(['&', 
  '|', ('surname', '=', False), ('surname', '=', ''), 
  '|', ('second_surname', '=', False), ('second_surname', '=', ''),
  '|', ('first_surname', '!=', False), ('first_surname', '!=', '')])
for partner in partners: partner.lastname = partner.first_surname