import erppeek
import credentials as cred
c = erppeek.Client(server=cred.server,db=cred.database,user=cred.user,password=cred.password)
# Compute new data
contribs = c.model('sm_contributions.sm_contribution').browse([])
for contrib in contribs:
  contrib.compute_contribution_email_template()